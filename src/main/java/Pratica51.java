import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;
import utfpr.ct.dainf.if62c.pratica.MatrizesIncompativeisException;
import utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException;
import utfpr.ct.dainf.if62c.pratica.SomaMatrizesIncompativeisException;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica51 {

    public static void main(String[] args) {
        
        try {
            Matriz m1 = new Matriz(3,2);
         
            m1.addValor(0, 0, 1.1);
            m1.addValor(0, 1, 1.2);
            m1.addValor(1, 0, 2.1);
            m1.addValor(1, 1, 2.2);
            m1.addValor(2, 0, 3.1);
            m1.addValor(2, 1, 3.2);
        
            Matriz m2 = new Matriz(3,3);

            m2.addValor(0, 0, 1.1);
            m2.addValor(0, 1, 1.2);
            m2.addValor(0, 2, 1.3);
            m2.addValor(1, 0, 2.1);
            m2.addValor(1, 1, 2.2);
            m2.addValor(1, 2, 2.3);
            m2.addValor(2, 0, 3.1);
            m2.addValor(2, 1, 3.2);
            m2.addValor(2, 2, 3.3);
        
            Matriz matrizProd = m1.prod(m2);

            Matriz matrizSoma = m1.soma(m2);
        
        
            System.out.println("Matriz 1:\n" + m1);

            System.out.println("\nMatriz 2:\n" + m2);

            System.out.println("\nMatriz Soma:\n" + matrizSoma);

            System.out.println("\nMatriz Produto:\n" + matrizProd);

        } catch (MatrizInvalidaException | ProdMatrizesIncompativeisException | SomaMatrizesIncompativeisException e){
            System.out.println(e);
        }
    }
}
