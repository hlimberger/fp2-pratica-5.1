/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author henrique
 */
public class SomaMatrizesIncompativeisException extends MatrizesIncompativeisException {

    
    public SomaMatrizesIncompativeisException(Matriz m1, Matriz m2){
        super(String.format("Matrizes de %dx%d e %dx%d não podem ser somadas",
                            m1.getLinhas(),m1.getColunas(),
                            m2.getLinhas(),m2.getColunas()),m1,m2);
    }
}
